# Návody

Řada služeb obsahuje podporu pro externí autentizaci poskytovatelem identit skrze autentizační protokoly SAML2 nebo OpenID Connect. Konfigurace je ovšem v praxi často složitá, liší se případ od případu a vyžaduje alespoň základní znalost použitého autentizačního protokolu. Tato dokumentace popisuje vhodnou konfiguraci vybraných softwarových produktů.

## Návody pro konfiguraci

Zvolte nejvhodnější variantu pro Váš případ:

### ![](icons/desktop-solid.png) Frontendové aplikace (např. JavaScript)

- [Expo AuthSession](docs-cs/Frontendov%C3%A9%20aplikace/OIDC/expo_authsession.md) (OIDC)

### ![](icons/code-solid.png) Vlastní aplikace na straně serveru (např. PHP, Python)

- OIDC + PHP: [PHP OpenID Connect Basic Client](docs-cs/Vlastn%C3%AD%20aplikace/OIDC%20%2B%20PHP/php_openid_connect_basic_client.md)
- OIDC + Python Django: [mozilla-django-oidc](docs-cs/Vlastn%C3%AD%20aplikace/OIDC%20%2B%20Python%20Django/mozilla-django-oidc.md)
- SAML + PHP: [SimpleSAMLphp](docs-cs/Vlastn%C3%AD%20aplikace/SAML%20%2B%20PHP/simplesamlphp.md)

### ![](icons/server-solid.png) Hotové aplikace provozované na vlastních serverech

- [GitLab](docs-cs/Hotov%C3%A9%20aplikace/SAML/gitlab.md) (SAML)
- [Sentry](docs-cs/Hotov%C3%A9%20aplikace/SAML/sentry.md) (SAML)

### ![](icons/globe-solid.png) Jiné aplikace bez integrace OIDC nebo SAML

#### Apache web server

- [mod_auth_openidc](docs-cs/Jin%C3%A9%20aplikace/Apache%20web%20server/mod_auth_openidc.md) (OIDC)
- [mod_oauth2](docs-cs/Jin%C3%A9%20aplikace/Apache%20web%20server/mod_oauth2.md) (OAuth)
- [Shibboleth SP 3](docs-cs/Jin%C3%A9%20aplikace/Apache%20web%20server/shibboleth_sp.md) (SAML)

#### Jiný nebo žádný web server

- [OAuth2 Proxy](docs-cs/Jin%C3%A9%20aplikace/Jin%C3%BD%20nebo%20%C5%BE%C3%A1dn%C3%BD%20web%20server/oauth2_proxy.md)
