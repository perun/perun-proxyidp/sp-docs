# sp-docs

Documentation to be published by an Identity Provider for its Service Providers on how to connect common specific software to SAML or OIDC.

## Requirements

This documentation expects that the Identity Provider supports:

- SAML2
    - HTTP-Redirect binding
    - HTTP-POST binging
    - signed responses
    - public URL with metadata in XML
- OIDC
    - well-known endpoint
    - authorization endpoint
    - public URL with keys in JWKS
    - token endpoint
        - optional basic authentication using client secret
    - userinfo endpoint
    - Proof Key for Code Exchange aka PKCE (S256)
    - standard scopes and corresponding claims (`openid`, `email`, `profile`)
    - refresh tokens

## Prepare

This documentation is intended to be modified with specific information about the Identity Provider (IdP),
and then published for Service Providers to see.

Copy the files or clone the repository and replace all placeholders mentioned in the [list of placeholders](./placeholders.md).

For example, to replace `%OIDC_ISSUER%` with `https://oidc.muni.cz/oidc/`, run:

```sh
find . -type f -name "*.md" -exec sed -i 's/%OIDC_ISSUER%/https:\/\/oidc.muni.cz\/oidc\//g' {} +
```

## Deploy

You can publish the resulting documentation in multiple ways.

### Raw

The most simple is just to make the GitHub/GitLab repository with the Markdown files public, hyperlinks will work between pages.

You can open [English](guide-en.md) or [Czech](guide-cs.md) version directly.

### Convert

You can easily convert the Markdown files using various online and offline tools, for example into HTML or PDF.

### mkdocs

To create a simple website with the documentation, you can use [mkdocs](https://www.mkdocs.org/user-guide/deploying-your-docs/).

To prepare, install [Python 3](https://www.python.org/downloads/) and [pip](https://pip.pypa.io/en/stable/installation/) and run:

```sh
pip3 install -r requirements.txt
```

Then run a local live server:

```sh
mkdocs serve
```

or build the site:

```sh
mkdocs build
```

You can [publish](https://www.mkdocs.org/user-guide/deploying-your-docs/) to GitHub Pages, Read the Docs or any web server.

You can also upload the result to GitLab Pages by using the [.gitlab-ci.yml](.gitlab-ci.yml) in this repository.
