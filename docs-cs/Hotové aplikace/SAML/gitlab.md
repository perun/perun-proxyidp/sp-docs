# GitLab

GitLab je open source platforma umožňující správu a analýzu zdrojového kódu, kterou lze provozovat na vlastním serveru.

## Výchozí stav

- Tento návod předpokládá, že máte nainstalovanou vlastní GitLab instanci na adrese `https://sp.example.org/`.

## Konfigurace

1. Povolte SAML [předepsaným postupem](https://docs.gitlab.com/ee/integration/saml.html) podle Vašeho způsobu instalace GitLabu. Použijte následující argumenty a hodnoty:
   - `assertion_consumer_service_url: https://sp.example.org/users/auth/saml/callback`
   - `idp_cert_fingerprint: %SAML_SHA1_CERT_FINGERPRINT%`
   - `idp_sso_target_url: %SAML_SSO_HTTP_REDIRECT_URL%`
   - `issuer: https://sp.example.org/users/auth/saml/metadata`
   - `name_identifier_format: urn:oasis:names:tc:SAML:2.0:nameid-format:persistent`
2. K argumentům v předchozím bodě přidejte následující:
   - `attribute_statements: { email: ['urn:oid:0.9.2342.19200300.100.1.3'], first_name: ['urn:oid:2.5.4.42'], last_name: ['urn:oid:2.5.4.4'], name: ['urn:oid:2.16.840.1.113730.3.1.241'] }`
3. Volitelné: pokud chcete použít atribut z IdP jako uživatelské jméno v GitLabu (typicky uid), přidejte následující atribut do **attribute_statements**:
   - `attribute_statements: { email: ['urn:oid:0.9.2342.19200300.100.1.3'], first_name: ['urn:oid:2.5.4.42'], last_name: ['urn:oid:2.5.4.4'], name: ['urn:oid:2.16.840.1.113730.3.1.241'], nickname: ['urn:oid:1.2.840.113549.1.9.2'] }`

## Podklady k registraci

- Vaše **entity ID** bude shodné s **metadata URL**: `https://sp.example.org/users/auth/saml/metadata`

## Další zdroje

- Více možností konfigurace: [https://docs.gitlab.com/ee/integration/saml.html](https://docs.gitlab.com/ee/integration/saml.html)
