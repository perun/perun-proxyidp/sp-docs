# Sentry

Sentry je open source nástroj pro monitorování a sběr chyb, který lze provozovat na vlastním serveru.

## Výchozí stav

- Tento návod předpokládá, že máte na adrese `https://sp.example.org/` nainstalované self-hosted Sentry podle [běžného postupu](https://develop.sentry.dev/self-hosted/) a že v něm máte vytvořenou organizaci.
    - Pokud používáte více organizací v Sentry, je potřeba každou připojit zvlášť.
    - Pokud používáte službu sentry.io, za adresu `https://sp.example.org/` si v následujících krocích dosaďte adresu `https://sentry.io/`.

## Konfigurace

Připojení je možné následujícím způsobem prostřednictvím protokolu SAML.

1. V administraci Sentry zvolte **User Authentication > SSO**.
2. Do pole **Metadata URL** vložte %SAML_METADATA_URL%.
3. Klikněte na tlačítko **Get metadata**.
   - Pokud dojde k chybě, nemá Sentry přístup k internetu (např. kvůli firewallu). V takovém případě postupujte následovně:
     1. Zvolte záložku **XML**.
     2. Stáhněte obsah souboru z %SAML_METADATA_URL% a vložte jej do pole **Metadata XML**.
     3. Potvrďte tlačítkem **Parse Metadata**.
4. Na následující stránce **Map Identity Provider Attributes** vyplňte pole takto:
   1. **IdP User ID**: `urn:oid:1.3.6.1.4.1.5923.1.1.1.13`
   2. **User Email**: `urn:oid:0.9.2342.19200300.100.1.3`
   3. **First Name**: `urn:oid:2.5.4.42`
   4. **Last Name**: `urn:oid:2.5.4.4`

## Podklady k registraci

- Vaše **entity ID** bude shodné s **metadata URL**: `https://sentry.io/saml/metadata/[organization_slug]/`, kde `[organization_slug]` je zkratka organizace v Sentry

## Další zdroje

- Více informací k integraci: [https://docs.sentry.io/product/accounts/sso/saml2/](https://docs.sentry.io/product/accounts/sso/saml2/)
