# OAuth2 Proxy

Jedná se o reverzní proxy pro připojení aplikace, webu či API, které nemají podporu OIDC autentizace. Je možné ji použít samostatně nebo např. v kombinaci s nginx reverzní proxy. Pokud používáte Apache, použijte raději [mod_auth_openidc](../Apache%20web%20server/mod_auth_openidc.md).

## Výchozí stav

- Tento návod předpokládá, že váš server má adresu `https://sp.example.org/` a běží na něm aplikace nebo API s HTTP rozhraním na portu 8080.

## Podklady k registraci

- Nejdříve proveďte registraci OIDC klienta, na základě které získáte **client_id** (YOUR_CLIENT_ID) a **client_secret** (YOUR_CLIENT_SECRET). V registraci uveďte následující údaje:
    - **redirect_uri**: `https://sp.example.org/oauth2/callback`
    - **scopes**: openid, email
    - **authentication flow**: authorization code flow

## Konfigurace

1. Proveďte krok 1 z [odkazovaného návodu](https://oauth2-proxy.github.io/oauth2-proxy/docs/).
2. Vytvořte soubor **/etc/oauth2-proxy.cfg** s následujícím obsahem:

    ```ini
    upstreams = [
        "http://127.0.0.1:8080/"
    ]
    provider = "oidc"
    oidc_issuer_url = "%OIDC_ISSUER%"
    scope = "openid email"
    insecure_oidc_allow_unverified_email = true
    client_id = "YOUR_CLIENT_ID"
    client_secret = "YOUR_CLIENT_SECRET"
    insecure_oidc_skip_nonce = false
    session_cookie_minimal = true
    email_domains = "*"
    skip_provider_button = true
    whitelist_domains = [
        "%OIDC_HOSTNAME%"
    ]
    ```

3. Třetí krok se odvíjí od toho, zda používáte reverzní proxy.
    1. Pokud používáte **nginx** jako **reverzní proxy společně s OAuth2 Proxy**, na konec vytvářeného souboru přidejte následující řádky a dále postupujte podle [odkazovaných instrukcí](https://oauth2-proxy.github.io/oauth2-proxy/docs/configuration/overview#configuring-for-use-with-the-nginx-auth_request-directive) specifických pro tento případ:

        ```ini
        reverse_proxy = true
        http_address = "127.0.0.1:4180"
        set_xauthrequest = true
        ```

    2. Pokud nginx jako reverzní proxy nepoužíváte a spouštíte **pouze OAuth2 Proxy**, přidejte následující řádky na konec vytvářeného souboru (kde **/opt/https-cert.pem** a **/opt/https-key.pem** jsou certifikát a klíč pro HTTPS vašeho serveru sp.example.org):

        ```ini
        tls_cert_file = "/opt/https-cert.pem"
        tls_key_file = "/opt/https-key.pem"
        http_address = "sp.example.com:80"
        https_address = "sp.example.com:443"
        ```

4. [Vygenerujte **cookie_secret**](https://oauth2-proxy.github.io/oauth2-proxy/docs/configuration/overview/#generating-a-cookie-secret), např. tímto příkazem:

    ```sh
    python -c 'import os,base64; print(base64.urlsafe_b64encode(os.urandom(32)).decode())'
    ```

5. Do souboru **/etc/oauth2-proxy.cfg** přidejte následující adresu, kde YOUR_GENERATED_SECRET je vaše vygenerovaná hodnota:

    ```ini
    cookie_secret = "YOUR_GENERATED_SECRET"
    ```

6. Pokud vaše aplikace poskytuje API a chcete umožnit jeho volání pomocí OIDC bearer tokenů, přidejte následující řádek do souboru **/etc/oauth2-proxy.cfg**:

    ```ini
    skip_jwt_bearer_tokens = true
    ```

Tím umožníte použití tokenů vydaných pro client_id vaší aplikace (YOUR_CLIENT_ID).

## Další zdroje

- Další možnosti konfigurace: [https://oauth2-proxy.github.io/oauth2-proxy/docs/configuration/overview](https://oauth2-proxy.github.io/oauth2-proxy/docs/configuration/overview)
