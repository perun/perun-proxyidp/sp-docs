# mod_oauth2

Jedná se o modul pro Apache web server pro připojení API bez zabudované podpory OIDC autentizace. API bude chráněno OIDC bearer tokenem.

## Výchozí stav

- Tento návod předpokládá, že na vašem serveru na adrese `https://sp.example.org/` běží Apache (veřejně) a API (neveřejně).

## Podklady k registraci

- Nejdříve proveďte registraci OAuth2 nebo OIDC klienta, na základě které získáte **client_id** (YOUR_CLIENT_ID) a **client_secret** (YOUR_CLIENT_SECRET). V registraci uveďte následující údaje:
    - **redirect_uri**: není potřeba (pokud je vyžadováno, uveďte např. `https://sp.example.org/`)
    - **scopes**: openid
    - **token introspection**: ano

## Konfigurace

1. Nainstalujte následující balíčky (v některých Linuxových distribucích dostupné z apt repozitářů):
   1. [liboauth2](https://github.com/zmartzone/liboauth2/releases)
   2. [mod_oauth2](https://github.com/zmartzone/mod_oauth2/releases)
2. Do konfigurace **Apache VirtualHost** (většinou v **/etc/apache2/sites-enabled/\*.conf**), který zpřístupňuje API (např. blok Location obsahující ProxyPass) jenž má být chráněno autentizací OIDC tokenem, přidejte následující:

```apacheconf
   AuthType oauth2
   OAuth2TokenVerify metadata %OIDC_WELL_KNOWN_URI% introspect.auth=client_secret_basic&client_id=YOUR_CLIENT_ID&client_secret=YOUR_CLIENT_SECRET
   OAuth2AcceptTokenIn header name=Authorization&type=bearer
   require valid-user
```

## Další zdroje

- Ukázková konfigurace s dalšími možnostmi nastavení: [https://github.com/zmartzone/mod_oauth2/blob/master/oauth2.conf](https://github.com/zmartzone/mod_oauth2/blob/master/oauth2.conf)
