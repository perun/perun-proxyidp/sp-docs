# Shibboleth SP 3

Shibboleth Service Provider je komponenta pro připojení aplikace k poskytovateli identit protokolem SAML2.

> **!** Pokud chcete připojit vaši službu do **eduID.cz** nebo do federace **eduGAIN**, můžete postupovat podle
> [návodu, který je přímo věnován tomuto konkrétnímu případu](https://www.eduid.cz/cs/tech/sp/shibboleth).

## Výchozí stav

- Tento návod předpokládá, že už máte [nainstalovaný](https://shibboleth.atlassian.net/wiki/spaces/SP3/pages/2065335537/Installation) a [nastavený](https://shibboleth.atlassian.net/wiki/spaces/SP3/pages/2065335529/GettingStarted) Shibboleth SP 3 dle odkazovaných instrukcí.
- Budete potřebovat pouze jednoho IdP (poskytovatele identity).

## Konfigurace

1. Přidejte nebo upravte adresu `MetadataProvider` v souboru **shibboleth2.xml**:

    ```xml
    <MetadataProvider type="XML" uri="%SAML_METADATA_URL%"
    backingFilePath="/etc/shibboleth/idp-metadata.xml" reloadInterval="7200"> </MetadataProvider>
    ```

2. Změňte v tagu `<SSO...>` adresu následujícím způsobem:

    ```xml
    <SSO entityID="%SAML_ENTITY_ID%"> SAML2 </SSO>
    ```

## Podklady k registraci

- Vaše **metadata URL** bude: `https://sp.example.org/Shibboleth.sso/Metadata`
- Vaše **entity ID** si můžete nastavit v **shibboleth2.xml** v parametru `<ApplicationDefaults entityID="https://sp.example.org/shibboleth"`
    - V tomto příkladu by vaším entity ID bylo `https://sp.example.org/shibboleth`
