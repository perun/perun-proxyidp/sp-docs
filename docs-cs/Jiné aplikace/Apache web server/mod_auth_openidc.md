# mod_auth_openidc

Jedná se o modul pro Apache web server pro připojení aplikace nebo webu bez zabudované podpory OIDC autentizace.

## Výchozí stav

- Tento návod předpokládá, že na vašem serveru na adrese `https://sp.example.org/` běží Apache (veřejně) a aplikace/web (neveřejně).

## Podklady k registraci

- Nejdříve proveďte registraci OIDC klienta, na základě které získáte **client_id** (YOUR_CLIENT_ID) a **client_secret** (YOUR_CLIENT_SECRET). V registraci uveďte následující údaje:
    - **redirect_uri**: `https://sp.example.org/oauth2callback`
    - **scopes**: openid email profile
    - **authetication flow**: authorization code flow
        - **PKCE** (pokud je podporováno): S256
    - **back-channel logout URI** (pokud je podporováno): `https://sp.example.org/oauth2callback?logout=backchannel`
    - **token endpoint authentication**: client_secret_basic

## Konfigurace

1. Nainstalujte následující balíčky (v některých Linuxových distribucích dostupné z apt repozitářů):
    1. [mod_auth_openidc](https://github.com/zmartzone/mod_auth_openidc/releases)
2. Do konfigurace **Apache VirtualHost** (většinou v **/etc/apache2/sites-enabled/\*.conf**) vložte následující:

    ```apacheconf
    OIDCProviderMetadataURL %OIDC_WELL_KNOWN_URI%
    OIDCClientID YOUR_CLIENT_ID
    OIDCClientSecret YOUR_CLIENT_SECRET
    OIDCCryptoPassphrase "exec:/bin/bash -c \"head /dev/urandom | tr -dc A-Za-z0-9 | head -c 32\""
    OIDCScope "openid email profile"
    # timeout pro neaktivní session, default je 300 vteřin
    OIDCSessionInactivityTimeout 86400
    # timeout pro aktivní session, default je 28800 vteřin
    OIDCSessionMaxDuration 86400

    # neexistující URL pro návrat z OIDC serveru
    OIDCRedirectURI "/oauth2callback"
    <Location "/oauth2callback">
        AuthType openid-connect
        Require valid-user
    </Location>
    ```

3. Pokud je podporováno PKCE, přidejte:

    ```apacheconf
    OIDCPKCEMethod S256
    ```

4. Do části Apache konfigurace, která zpřístupňuje aplikaci/web (např. blok Location, který obsahuje direktivu ProxyPass), přidejte následující direktivy pro povolení všech autentizovaných uživatelů:

    ```apacheconf
    AuthType openid-connect
    Require valid-user
    ```

    Pokud potřebujete provádět nějakou autorizaci, například povolit pouze uživatele s existujícím username, použijte direktivy podobné těmto:

    ```apacheconf
    <Directory /var/www/wiki>
        AuthType openid-connect
        <RequireAll>
            Require claim "preferred_username~.+"
        </RequireAll>
        ErrorDocument 401 /missing-username.html
    </Directory>
    ```

## Další zdroje

Ukázková konfigurace s dalšími možnostmi nastavení: [https://github.com/OpenIDC/mod_auth_openidc/blob/master/auth_openidc.conf](https://github.com/OpenIDC/mod_auth_openidc/blob/master/auth_openidc.conf)
