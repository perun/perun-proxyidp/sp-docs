# Návody

Řada služeb obsahuje podporu pro externí autentizaci poskytovatelem identit skrze autentizační protokoly SAML2 nebo OpenID Connect. Konfigurace je ovšem v praxi často složitá, liší se případ od případu a vyžaduje alespoň základní znalost použitého autentizačního protokolu. Tato dokumentace popisuje vhodnou konfiguraci vybraných softwarových produktů.

## Návody pro konfiguraci

Zvolte nejvhodnější variantu pro Váš případ z menu:

![](icons/desktop-solid.png) Frontendové aplikace (např. JavaScript)

![](icons/code-solid.png) Vlastní aplikace na straně serveru (např. PHP, Python)

![](icons/server-solid.png) Hotové aplikace provozované na vlastních serverech

![](icons/globe-solid.png) Jiné aplikace bez integrace OIDC nebo SAML
