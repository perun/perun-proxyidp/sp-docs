# Expo AuthSession

AuthSession je knihovna pro framework Expo (React Native), která výrazně usnadňuje autentizaci protokolem OAuth nebo OIDC. Slouží tedy k připojení nativních či hybridních aplikací (mobilní aplikace s případnou webovou verzí).

## Výchozí stav

- Tento návod předpokládá, že máte projekt (aplikaci) ve frameworku Expo v režimu tzv. managed workflow.
    - Můžete ji vytvořit např. pomocí příkazu:

```sh
    npx create-expo-app my-app
```

- Zvolte si schéma pro odkazy, např. "myapp" (krátký řetězec složený pouze z malých písmen anglické abecedy). Pokud má aplikace webovou verzi, návod předpokládá adresu `https://sp.example.org/`.

## Podklady pro registraci

- Poté proveďte registraci OIDC klienta, na základě které získáte **client_id** (YOUR_CLIENT_ID). V registraci uveďte následující údaje:
    - **redirect_uri**:
        - `myapp://redirect`
        - `https://sp.example.org`
        - `exp://localhost:19000`
    - **scopes**: openid, profile, email
    - **authentication flow**: authorization code flow
    - **PKCE**: S256
    - **token endpoint authentication**: none
- Client_secret nebudete potřebovat.

## Konfigurace

1. [Nainstalujte](https://docs.expo.dev/versions/latest/sdk/auth-session/#installation) knihovnu **AuthSession** a její závislost:

   ```sh
   npx expo install expo-auth-session expo-random
   ```

2. V konfiguračním souboru (**app.config.js** nebo **app.json**) nastavte schéma pro odkazy, které jste si zvolili.

   - Např. pro app.json:

   ```json
   {
     "expo": {
       "scheme": "myapp"
     }
   }
   ```

3. Do kódu aplikace (v hlavním souboru **App.js**) přidejte tlačítko a logiku pro autentizaci, např. takto:

   ```js
   import * as React from 'react';
   import * as WebBrowser from 'expo-web-browser';
   import { makeRedirectUri, useAuthRequest, useAutoDiscovery } from 'expo-auth-session';
   import { Button, Platform } from 'react-native';

   WebBrowser.maybeCompleteAuthSession();

   const clientId = 'YOUR_CLIENT_ID';

   export default function App() {
      const [authTokens, setAuthTokens] = React.useState(null);
      const discoveryDocument = useAutoDiscovery('%OIDC_ISSUER%');
      const redirectUri = makeRedirectUri({native: 'myapp://redirect'});
      const [request, response, promptAsync] = useAuthRequest(
          {
              responseType: 'code',
               clientId,
              scopes: ['openid', 'profile', 'email'],
              redirectUri,
              usePKCE: true,
          },
          discoveryDocument
      );

      React.useEffect(() => {
          const exchangeFn = async (exchangeTokenReq) => {
              try {
                  const exchangeTokenResponse = await exchangeCodeAsync(
                      exchangeTokenReq,
                      discoveryDocument
                  );
                  setAuthTokens(exchangeTokenResponse);
              } catch (error) {
                  console.error(error);
              }
          };
          if (response) {
              if (response.error) {
                  Alert.alert.(
                      'Authentication error',
                      response.params.error_description || 'something went wrong'
                  );
                  return;
              }
              if (response.type === 'success') {
                  exchangeFn({
                      clientId,
                      code: response.params.code,
                      redirectUri,
                      extraParams: {
                          code_verifier: request.codeVerifier,
                      },
                  });
              }
          }
      }, [discoveryDocument, request, response]);

      return authTokens ? (
          <Button title="Logout" onPress={() => logout()} />
      ) : (
          <Button disabled={!request} title="Login" onPress={() => promptAsync()} />
      );
   }
   ```

4. Proměnná **authTokens** v ukázce nyní obsahuje access token uživatele, který je možné použít pro volání API chráněného OAuth/OIDC autentizací.
5. Volitelné: pro získání dat o uživateli (např. e-mailové adresy) použijte tento příkaz:

   ```js
   const userData = fetchUserInfoAsync(authTokens, discoveryDocument);
   ```

## Další zdroje

- Více informací naleznete v dokumentaci Expo AuthSession: [https://docs.expo.dev/versions/latest/sdk/auth-session/](https://docs.expo.dev/versions/latest/sdk/auth-session/)
- Příklad autentizace při použití React Navigation: [https://reactnavigation.org/docs/auth-flow](https://reactnavigation.org/docs/auth-flow)
