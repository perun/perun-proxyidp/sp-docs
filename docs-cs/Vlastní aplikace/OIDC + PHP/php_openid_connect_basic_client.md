# PHP OpenID Connect Basic Client

Jedná se o knihovnu v jazyce PHP, která poskytuje možnost OIDC autentizace pro webovou aplikaci či API.

## Výchozí stav

- Tento návod předpokládá, že máte aplikaci v jazyce PHP dostupnou na adrese `https://sp.example.org/`.

## Podklady k registraci

- Nejdříve proveďte registraci, na základě které získáte **client_id** (YOUR_CLIENT_ID) a **client_secret** (YOUR_CLIENT_SECRET). V registraci uveďte následující údaje:
    - **redirect_uri**: `https://sp.example.org/callback.php`
    - **scopes**: openid, profile, email
    - **authentication flow**: authorization code flow
    - **PKCE**: S256
    - **token endpoint authentication**: client_secret_basic

## Konfigurace

1. Nainstalujte knihovnu pomocí [composeru](https://getcomposer.org/download/):

    ```sh
    composer require jumbojett/openid-connect-php
    ```

2. Vytvořte soubor **callback.php** dostupný na adrese `https://sp.example.org/callback.php` s následujícím obsahem:

    ```php
    require __DIR__ . '/vendor/autoload.php';

    use Jumbojett\OpenIDConnectClient;

    $oidc = new OpenIDConnectClient('%OIDC_ISSUER%',
                                    'YOUR_CLIENT_ID',
                                    'YOUR_CLIENT_SECRET');
    $oidc->addScope(['openid', 'profile', 'email']);
    $oidc->setCodeChallengeMethod('S256');
    $oidc->authenticate();
    $userID = $oidc->getVerifiedClaims('sub');
    $userInfo = $oidc->requestUserInfo();
    ```

    Proměnná **$userID** zde obsahuje ID uživatele, který se přihlásil. Proměnná **$userInfo** obsahuje data o uživateli (jméno a email).

3. Volitelné: pokud část aplikace představuje API, která má být chráněné OAuth2 autentizací, přidejte na začátek souboru poskytujícího API následující:

    ```php
    require __DIR__ . '/vendor/autoload.php';

    use Jumbojett\OpenIDConnectClient;

    $oidc = new OpenIDConnectClient('%OIDC_ISSUER%',
                                    'YOUR_CLIENT_ID',
                                    'YOUR_CLIENT_SECRET');
    $headers = getallheaders();
    if (!empty($headers["Authorization"]) && preg_match('/^Bearer\s(\S+)$/', $headers['Authorization'], $matches)) {
        $data = $oidc->introspectToken($matches[1]);
    } else {
        $data = null;
    }

    if (!$data || !$data->active) {
        header('HTTP/1.1 401 Unauthorized');
        header('WWW-Authenticate: Bearer');
        exit;
    }
    $userID = $data->sub;
    ```

    Proměnná **$userID** zde obsahuje ID uživatele, který zavolal API.
