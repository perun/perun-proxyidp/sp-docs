# mozilla-django-oidc

Jedná se o knihovnu pro framework Python Django (a Django REST framework), která poskytuje možnost OIDC autentizace pro webovou aplikaci (či API).

## Výchozí stav

- Tento návod předpokládá, že vaše Django aplikace nebo API je dostupné na adrese `https://sp.example.org/`.

## Podklady k registraci

- Nejdříve proveďte registraci OIDC klienta, na základě které získáte **client_id** (YOUR_CLIENT_ID) a **client_secret** (YOUR_CLIENT_SECRET). V registraci uveďte následující údaje:
    - **redirect_uri**: `https://sp.example.org/oauth2/callback/`
    - **scopes**: openid, profile, email
    - **authetication flow**: authorization code flow
    - **PKCE**: none
    - **token endpoint authentication**: client_secret_basic

## Konfigurace

1. Aktivujte knihovnu [upravením souboru **settings.py**](https://mozilla-django-oidc.readthedocs.io/en/stable/installation.html#add-settings-to-settings-py):

    ```python
        # Add 'mozilla_django_oidc' to INSTALLED_APPS
        INSTALLED_APPS = (
            # ...
            'django.contrib.auth',
            'mozilla_django_oidc',  # Load after auth
            # ...
        )

        # Add 'mozilla_django_oidc' authentication backend
        AUTHENTICATION_BACKENDS = (
            'mozilla_django_oidc.auth.OIDCAuthenticationBackend',
            # ...
        )
    ```

2. Do stejného souboru **settings.py** přidejte následující řádky:

    ```python
    OIDC_RP_CLIENT_ID = "YOUR_CLIENT_ID"
    OIDC_RP_CLIENT_SECRET = "YOUR_CLIENT_SECRET"
    OIDC_OP_JWKS_ENDPOINT = "%OIDC_JWKS_URI%"
    OIDC_OP_AUTHORIZATION_ENDPOINT = "%OIDC_AUTHORIZATION_ENDPOINT%"
    OIDC_OP_TOKEN_ENDPOINT = "%OIDC_TOKEN_ENDPOINT%"
    OIDC_OP_USER_ENDPOINT = "%OIDC_USERINFO_ENDPOINT%"
    OIDC_RP_SIGN_ALGO = "RS256"
    OIDC_RP_SCOPES = "openid profile email"
    OIDC_TOKEN_USE_BASIC_AUTH = True
    OIDC_STORE_ID_TOKEN = True
    OIDC_VERIFY_KID = False
    ```

3. Upravte soubor **urls.py** přidáním argumentu `path('oidc/', include('mozilla_django_oidc.urls'))`:

    ```python
    from django.urls import path

    urlpatterns = [
        # ...
        path('oidc/', include('mozilla_django_oidc.urls')),
        # ...
    ]
    ```

4. Rozhodněte se, jak se budou uživatelské účty v Django aplikaci párovat na data z přihlášení a postupujte podle [odkazovaného návodu](https://mozilla-django-oidc.readthedocs.io/en/stable/installation.html#connecting-oidc-user-identities-to-django-users).
5. Rozhodněte se, jestli se mají uživatelské účty vytvářet automaticky každému přihlášenému uživateli:
    - Pokud **ano**, postupujte podle [odkazovaných kroků](https://mozilla-django-oidc.readthedocs.io/en/stable/installation.html#creating-django-users).
    - Pokud **ne**, přidejte pouze následující řádek do **settings.py**:

        ```python
        OIDC_CREATE_USER = False
        ```

6. Pokud má vaše aplikace webové rozhraní, [vložte přihlašovací tlačítko nebo odkaz do stránky](https://mozilla-django-oidc.readthedocs.io/en/stable/installation.html#enable-login-and-logout-functionality-in-templates), např. takto:

    ```html
    {% if user.is_authenticated %}
        <p>Current user: {{ user.email }}</p>
        <form action="{% url 'oidc_logout' %}" method="post">
            {% csrf_token %} <input type="submit" value="logout" />
        </form>
    {% else %}
        <a href="{% url 'oidc_authentication_init' %}">Login</a>
    {% endif %}
    ```

7. Pokud vaše aplikace poskytuje API pomocí Django REST framework, pokračujte jeho integrací podle [tohoto postupu](https://mozilla-django-oidc.readthedocs.io/en/stable/drf.html).

## Další zdroje

- Volitelné možnosti konfigurace při instalaci: [https://mozilla-django-oidc.readthedocs.io/en/stable/installation.html#additional-optional-configuration](https://mozilla-django-oidc.readthedocs.io/en/stable/installation.html#additional-optional-configuration)
- Více možností dalšího nastavení: [https://mozilla-django-oidc.readthedocs.io/en/stable/settings.html](https://mozilla-django-oidc.readthedocs.io/en/stable/settings.html)
