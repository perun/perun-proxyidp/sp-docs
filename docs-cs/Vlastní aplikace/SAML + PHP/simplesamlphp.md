# SimpleSAMLphp

SimpleSAMLphp je knihovna v jazyce PHP pro připojení aplikace k poskytovateli identit protokolem SAML2.

> **!** Pokud chcete připojit vaši službu do **eduID.cz** nebo do federace **eduGAIN**, můžete postupovat podle
> [návodu, který je přímo věnován tomuto konkrétnímu případu](https://www.eduid.cz/cs/tech/sp/simplesamlphp).

## Výchozí stav

- Tento návod předpokládá, že už máte nastavený SimpleSAMLphp podle [odkazovaných instrukcí](https://simplesamlphp.org/docs/stable/simplesamlphp-sp.html).

## Konfigurace

1. Nastavte poskytovatele služby.
    1. Připravte si pár klíčů, např. **/var/simplesamlphp/cert/example.key** a **/var/simplesamlphp/cert/example.crt**.
    2. Upravte **config/authsources.php**:

        ```php
        // ...
        'default-sp' => [
            'saml:SP',
            'certificate' => 'example.crt',
            'privatekey' => 'example.key',
            'idp' => '%SAML_ENTITY_ID%',
            'redirect.sign' => true,
            'redirect.validate' => true,
            'assertion.encryption' => true,
            'sign.authnrequest' => true,
            'sign.logout' => true,
            'validate.logout' => true,
            'WantAssertionsSigned' => true,
            'contacts' => [
                'contactType' => 'technical',
                'emailAddress' => 'admin@sp.example.org',
            ],
        ],
        // ...
        ```

2. Nastavte automatický refresh metadat.
    1. Povolte [dle návodu](https://simplesamlphp.org/docs/contrib_modules/metarefresh/simplesamlphp-automated_metadata.html) moduly **cron** a **metarefresh**.
    2. Nahraďte obsah souboru **config/config-metarefresh.php**:

        ```php
        <?php
        $config = [
            'sets' => [
                'idp' => [
                    'cron' => ['daily'],
                    'sources' => [
                        [
                            'src' => '%SAML_METADATA_URL%',
                        ],
                    ],
                'expireAfter' => 60 * 60 * 24 * 4, // Maximum 4 days cache time.
                'outputDir' => 'metadata/metarefresh/',
                'outputFormat' => 'flatfile',
                'types' => ['saml20-idp-remote'],
                ],
            ],
        ];
        ```

3. Přidejte zdroj metadat v **config/config.php**:

    ```php
    // ...
    'metadata.sources' => [
        ['type' => 'flatfile'],
        ['type' => 'flatfile', 'directory' => 'metadata/metarefresh'],
    ],
    // ...
    ```

## Podklady k registraci

- Vaše **entity ID** bude shodné s **metadata URL**: `https://sp.example.org/simplesaml/module.php/saml/sp/metadata.php/default-sp`
