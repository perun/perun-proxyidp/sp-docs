# mozilla-django-oidc

A library for the Python Django framework (and Django REST framework), which allows OIDC authentication for a web application or API.

## Initial state

- This guide assumes that your Django application or API is available at `https://sp.example.org/`.

## Registration information

- First perform the OIDC client registration, upon which you will recieve a **client_id** (YOUR_CLIENT_ID) and a **client_secret** (YOUR_CLIENT_SECRET). Use the following in the registration process:
    - **redirect_uri**: `https://sp.example.org/oauth2/callback/`
    - **scopes**: openid, profile, email
    - **authetication flow**: authorization code flow
    - **PKCE**: none
    - **token endpoint authentication**: client_secret_basic

## Configuration

1. Activate the library by [editing the **settings.py** file](https://mozilla-django-oidc.readthedocs.io/en/stable/installation.html#add-settings-to-settings-py):

    ```python
    # Add 'mozilla_django_oidc' to INSTALLED_APPS
    INSTALLED_APPS = (
        # ...
        'django.contrib.auth',
        'mozilla_django_oidc',  # Load after auth
        # ...
    )
    
    # Add 'mozilla_django_oidc' authentication backend
    AUTHENTICATION_BACKENDS = (
        'mozilla_django_oidc.auth.OIDCAuthenticationBackend',
        # ...
    )
    ```

2. Add the following lines to the same **settings.py** file:

    ```python
    OIDC_RP_CLIENT_ID = "YOUR_CLIENT_ID"
    OIDC_RP_CLIENT_SECRET = "YOUR_CLIENT_SECRET"
    OIDC_OP_JWKS_ENDPOINT = "%OIDC_JWKS_URI%"
    OIDC_OP_AUTHORIZATION_ENDPOINT = "%OIDC_AUTHORIZATION_ENDPOINT%"
    OIDC_OP_TOKEN_ENDPOINT = "%OIDC_TOKEN_ENDPOINT%"
    OIDC_OP_USER_ENDPOINT = "%OIDC_USERINFO_ENDPOINT%"
    OIDC_RP_SIGN_ALGO = "RS256"
    OIDC_RP_SCOPES = "openid profile email"
    OIDC_TOKEN_USE_BASIC_AUTH = True
    OIDC_STORE_ID_TOKEN = True
    OIDC_VERIFY_KID = False
    ```

3. Edit the **urls.py** file by adding the argument `path('oidc/', include('mozilla_django_oidc.urls'))`:

    ```python
    from django.urls import path

    urlpatterns = [
        # ...
        path('oidc/', include('mozilla_django_oidc.urls')),
        # ...
    ]
    ```

4. Decide on how will the user accounts in the Django application be paired to the login data and the follow the [linked instructions](https://mozilla-django-oidc.readthedocs.io/en/stable/installation.html#connecting-oidc-user-identities-to-django-users).
5. Decide on whether user accounts should be assigned automatically to every logged in user.
    - If **yes**, follow the [referred instructions](https://mozilla-django-oidc.readthedocs.io/en/stable/installation.html#creating-django-users).
    - If **no**, add the following line to the **settings.py** file:

        ```python
        OIDC_CREATE_USER = False
        ```

6. If your application has a web interface, [create a login button or link](https://mozilla-django-oidc.readthedocs.io/en/stable/installation.html#enable-login-and-logout-functionality-in-templates).
7. If your application provides an API through the Django REST framework, continue with its [integration process](https://mozilla-django-oidc.readthedocs.io/en/stable/drf.html).

## Additional resources

- Additional optional configuration: [https://mozilla-django-oidc.readthedocs.io/en/stable/installation.html#additional-optional-configuration](https://mozilla-django-oidc.readthedocs.io/en/stable/installation.html#additional-optional-configuration)
- More settings options: [https://mozilla-django-oidc.readthedocs.io/en/stable/settings.html](https://mozilla-django-oidc.readthedocs.io/en/stable/settings.html)
