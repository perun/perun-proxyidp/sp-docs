# SimpleSAMLphp

SimpleSAMLphp is a PHP library that allows connecting applications to an Identity Provider through the SAML2 protocol.

## Initial state

- This guide assumes that you have already set up SimpleSAMLphp according to the [linked instructions](https://simplesamlphp.org/docs/stable/simplesamlphp-sp.html).

## Configuration

1. Set up the Service Provider.
    1. Create a key pair, e.g. **/var/simplesamlphp/cert/example.key** and **/var/simplesamlphp/cert/example.crt**.
    2. Edit **config/authsources.php**:

        ```php
        // ...
        'default-sp' => [
            'saml:SP',
            'certificate' => 'example.crt',
            'privatekey' => 'example.key',
            'idp' => '%SAML_ENTITY_ID%',
            'redirect.sign' => true,
            'redirect.validate' => true,
            'assertion.encryption' => true,
            'sign.authnrequest' => true,
            'sign.logout' => true,
            'validate.logout' => true,
            'WantAssertionsSigned' => true,
            'contacts' => [
                'contactType' => 'technical',
                'emailAddress' => 'admin@sp.example.org',
            ],
        ],
        // ...
        ```

2. Set up automatic metadata refresh.
    1. Follow [the instructions](https://simplesamlphp.org/docs/contrib_modules/metarefresh/simplesamlphp-automated_metadata.html) to allow the **cron** and **metarefresh** modules.
    2. Replace the contents of the **config/config-metarefresh.php** file:

        ```php
        <?php
            $config = [
                'sets' => [
                    'idp' => [
                        'cron' => ['daily'],
                        'sources' => [
                            [
                                'src' => '%SAML_METADATA_URL%',
                            ],
                        ],
                    'expireAfter' => 60 * 60 * 24 * 4, // Maximum 4 days cache time.
                    'outputDir' => 'metadata/metarefresh/',
                    'outputFormat' => 'flatfile',
                    'types' => ['saml20-idp-remote'],
                    ],
                ],
            ];
        ```

3. Add a metadata source in **config/config.php**:

    ```php
    // ...
    'metadata.sources' => [
        ['type' => 'flatfile'],
        ['type' => 'flatfile', 'directory' => 'metadata/metarefresh'],
    ],
    // ...
    ```

## Registration information

- Your **entity ID** will be the same as your **metadata URL**: `https://sp.example.org/simplesaml/module.php/saml/sp/metadata.php/default-sp`
