# PHP OpenID Connect Basic Client

A PHP language library which allows OIDC authentication for a web application or API.

## Initial state

- This guide assumes that you have a PHP application located at `https://sp.example.org/`.

## Registration information

- First perform the registration, upon which you will recieve a **client_id** (YOUR_CLIENT_ID) and a **client_secret** (YOUR_CLIENT_SECRET). Use the following in the registration process:
    - **redirect_uri**: `https://sp.example.org/callback.php`
    - **scopes**: openid, profile, email
    - **authentication flow**: authorization code flow
    - **PKCE**: S256
    - **token endpoint authentication**: client_secret_basic

## Configuration

1. Install the library through the [composer](https://getcomposer.org/download/):

    ```sh
    composer require jumbojett/openid-connect-php
    ```

2. Create a **callback.php** file available at `https://sp.example.org/callback.php` with the following contents:

    ```php
    require __DIR__ . '/vendor/autoload.php';

    use Jumbojett\OpenIDConnectClient;

    $oidc = new OpenIDConnectClient('%OIDC_ISSUER%',
                                    'YOUR_CLIENT_ID',
                                    'YOUR_CLIENT_SECRET');
    $oidc->addScope(['openid', 'profile', 'email']);
    $oidc->setCodeChallengeMethod('S256');
    $oidc->authenticate();
    $userID = $oidc->getVerifiedClaims('sub');
    $userInfo = $oidc->requestUserInfo();
    ```

    Variable **$userID** here contains the ID of an user, who logged in. Variable **$userInfo** contains the user's data (name and email).

3. Optional: if a part of the application is an API that is to be protected by OAuth2 authentication, add the following to the beginning of the file providing the API:

    ```php
    require __DIR__ . '/vendor/autoload.php';

    use Jumbojett\OpenIDConnectClient;

    $oidc = new OpenIDConnectClient('%OIDC_ISSUER%',
                                    'YOUR_CLIENT_ID',
                                    'YOUR_CLIENT_SECRET');
    $headers = getallheaders();
    if (!empty($headers["Authorization"]) && preg_match('/^Bearer\s(\S+)$/', $headers['Authorization'], $matches)) {
        $data = $oidc->introspectToken($matches[1]);
    } else {
        $data = null;
    }

    if (!$data || !$data->active) {
        header('HTTP/1.1 401 Unauthorized');
        header('WWW-Authenticate: Bearer');
        exit;
    }
    $userID = $data->sub;
    ```

    Variable **$userID** here contains the ID of the user who called the API.
