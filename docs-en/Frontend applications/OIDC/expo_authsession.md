# Expo AuthSession

AuthSession is a library for the Expo framework that makes authentication through the OAuth or OIDC protocols significantly easier. It is used for integrating native or hybrid applications (mobile applications with possible web versions).

## Initial state

- This guide assumes that you have a project (application) in the Expo framework in managed workflow.
    - You can create it e.g. through the command:

        ```sh
        npx create-expo-app my-app
        ```

- Create a link scheme, e.g. "myapp" (a short string with only lowercase english characters). If your application has a web version, this guide assumes it is located at `https://sp.example.org`.

## Registration information

- First perform the OIDC client registration, upon which you will recieve a **client_id** (YOUR_CLIENT_ID). Use the following in the registration process:
    - **redirect_uri**:
        - `myapp://redirect`
        - `https://sp.example.org`
        - `exp://localhost:19000`
    - **scopes**: openid, profile, email
    - **authentication flow**: authorization code flow
    - **PKCE**: S256
    - **token endpoint authentication**: none
- You will not need a client_secret.

## Configuration

1. [Install](https://docs.expo.dev/versions/latest/sdk/auth-session/#installation) the **AuthSession** library
   and its dependency:

    ```sh
    npx expo install expo-auth-session expo-random
    ```

2. Set up the desired link scheme in the configuration file (**app.config.js** or **app.json**) - e.g. for app.json:

    ```json
    {
        "expo": {
            "scheme": "myapp"
        }
    }
    ```

3. Add a button and authentication logic in the application code (in the main **App.js** file), e.g. like so:

    ```js
    import * as React from 'react';
    import * as WebBrowser from 'expo-web-browser';
    import { makeRedirectUri, useAuthRequest, useAutoDiscovery } from 'expo-auth-session';
    import { Button, Platform } from 'react-native';

    WebBrowser.maybeCompleteAuthSession();

    const clientId = 'YOUR_CLIENT_ID';

    export default function App() {
      const [authTokens, setAuthTokens] = React.useState(null);
      const discoveryDocument = useAutoDiscovery('%OIDC_ISSUER%');
      const redirectUri = makeRedirectUri({native: 'myapp://redirect'});
      const [request, response, promptAsync] = useAuthRequest(
          {
              responseType: 'code',
               clientId,
              scopes: ['openid', 'profile', 'email'],
              redirectUri,
              usePKCE: true,
          },
          discoveryDocument
      );

      React.useEffect(() => {
          const exchangeFn = async (exchangeTokenReq) => {
              try {
                  const exchangeTokenResponse = await exchangeCodeAsync(
                      exchangeTokenReq,
                      discoveryDocument
                  );
                  setAuthTokens(exchangeTokenResponse);
              } catch (error) {
                  console.error(error);
              }
          };
          if (response) {
              if (response.error) {
                  Alert.alert.(
                      'Authentication error',
                      response.params.error_description || 'something went wrong'
                  );
                  return;
              }
              if (response.type === 'success') {
                  exchangeFn({
                      clientId,
                      code: response.params.code,
                      redirectUri,
                      extraParams: {
                          code_verifier: request.codeVerifier,
                      },
                  });
              }
          }
      }, [discoveryDocument, request, response]);

      return authTokens ? (
          <Button title="Logout" onPress={() => logout()} />
      ) : (
          <Button disabled={!request} title="Login" onPress={() => promptAsync()} />
      );
    }
    ```

4. The **authTokens** variable in the example above now contains the user's access token which can be used for calling an API protected by OAuth/OIDC authentication.
5. Optional: for obtaining more user data (e.g. email addresses) use the following command:

    ```js
    const userData = fetchUserInfoAsync(authTokens, discoveryDocument);
    ```

## Additional resources

- More information in Expo AuthSession documentation: [https://docs.expo.dev/versions/latest/sdk/auth-session/](https://docs.expo.dev/versions/latest/sdk/auth-session/)
- Sample authentication using React Navigation: [https://reactnavigation.org/docs/auth-flow](https://reactnavigation.org/docs/auth-flow)
