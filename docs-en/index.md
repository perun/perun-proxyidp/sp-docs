# Instructions

Many services include support for external authentication by an identity provider through SAML2 or OpenID Connect authentication protocols. However, configuration is often complex in practice, varies from case to case, and requires at least a basic understanding of the authentication protocol used. This documentation describes the appropriate configuration of selected software products.

## Configuration guides

First you need to choose the guide that fits your specific case from the menu:

![](icons/desktop-solid.png) Frontend applications (e.g. JavaScript)

![](icons/code-solid.png) Own server-side applications (e.g. PHP, Python)

![](icons/server-solid.png) Existing applications running on your own servers

![](icons/globe-solid.png) Other applications without OIDC or SAML integration
