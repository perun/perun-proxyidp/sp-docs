# Shibboleth SP 3

Shibboleth Service Provider is a component for connecting an application to an Identity Provider through the SAML2 protocol.

## Initial state

- This guide assumes that you already have [installed](https://shibboleth.atlassian.net/wiki/spaces/SP3/pages/2065335537/Installation) and [set up](https://shibboleth.atlassian.net/wiki/spaces/SP3/pages/2065335529/GettingStarted) Shibboleth SP 3 as per the linked instructions.
- You will need only a single IdP (Identity Provider).

## Configuration

1. Add or edit the `MetadataProvider` address in your **shibboleth2.xml** file.

    ```xml
    <MetadataProvider type="XML" uri="%SAML_METADATA_URL%"
    backingFilePath="/etc/shibboleth/idp-metadata.xml" reloadInterval="7200"> </MetadataProvider>
    ```

2. Change the `<SSO...>` tag the following way:

    ```xml
    <SSO entityID="%SAML_ENTITY_ID%"> SAML2 </SSO>
    ```

## Registration information

- Your **metadata URL** will be: `https://sp.example.org/Shibboleth.sso/Metadata`
- You can set up your **entity ID** in **shibboleth2.xml** in the `<ApplicationDefaults entityID="https://sp.example.org/shibboleth"` parameter
    - In this example, your entity ID would be `https://sp.example.org/shibboleth`
