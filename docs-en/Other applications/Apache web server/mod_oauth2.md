# mod_oauth2

An Apache web server module for integrating an API without built in OIDC authentication support. The API will be protected by an OIDC bearer token.

## Initial state

- This guide assumes that your server located at `https://sp.example.org/` is running Apache (public) and an API (private).

## Registration information

- First perform the OAuth2 or OIDC client registration, upon which you will recieve a **client_id** (YOUR_CLIENT_ID) and a **client_secret** (YOUR_CLIENT_SECRET). Use the following in the registration process:
    - **redirect_uri**: not needed (if required, use e.g. `https://sp.example.org/`)
    - **scopes**: openid
    - **token introspection**: yes

## Configuration

1. Install the following packages (available from apt repositories of some Linux distributions):
    1. [liboauth2](https://github.com/zmartzone/liboauth2/releases)
    2. [mod_oauth2](https://github.com/zmartzone/mod_oauth2/releases)
2. Add the following to the **Apache VirtualHost** configuration (usually located in **/etc/apache2/sites-enabled/\*.conf**), which allows access the API (e.g. a Location block containing ProxyPass) that is to be protected by OIDC token authentication:

    ```apacheconf
    AuthType oauth2
    OAuth2TokenVerify metadata %OIDC_WELL_KNOWN_URI% introspect.auth=client_secret_basic&client_id=YOUR_CLIENT_ID&client_secret=YOUR_CLIENT_SECRET
    OAuth2AcceptTokenIn header name=Authorization&type=bearer
    require valid-user
    ```

## Additional resources

- Sample configuration with additional settings options: [https://github.com/zmartzone/mod_oauth2/blob/master/oauth2.conf](https://github.com/zmartzone/mod_oauth2/blob/master/oauth2.conf)
