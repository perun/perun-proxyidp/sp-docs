# mod_auth_openidc

An Apache web server module for integrating applications or websites without OIDC authentication support.

## Initial state

- This guide assumes that your server located at `https://sp.example.org/` is running Apache (public) and an application/website (private).

## Registration information

- First perform the OIDC client registration, upon which you will recieve a **client_id** (YOUR_CLIENT_ID) and a **client_secret** (YOUR_CLIENT_SECRET). Use the following in the registration process:
    - **redirect_uri**: `https://sp.example.org/oauth2callback`
    - **scopes**: openid email profile
    - **authetication flow**: authorization code flow
        - **PKCE** (if supported): S256
    - **back-channel logout URI** (pokud je podporováno): `https://sp.example.org/oauth2callback?logout=backchannel`
    - **token endpoint authentication**: client_secret_basic

## Configuration

1. Install the following packages (available from apt repositories of some Linux distributions):
    1. [mod_auth_openidc](https://github.com/zmartzone/mod_auth_openidc/releases)
2. Add the following to the **Apache VirtualHost** configuration (usually located in **/etc/apache2/sites-enabled/\*.conf**)

    ```apacheconf
    OIDCProviderMetadataURL https://id.muni.cz/oidc/.well-known/openid-configuration
    OIDCProviderMetadataRefreshInterval 3600
    OIDCClientID YOUR_CLIENT_ID
    OIDCClientSecret YOUR_CLIENT_SECRET
    OIDCCryptoPassphrase "exec:/bin/bash -c \"head /dev/urandom | tr -dc A-Za-z0-9 | head -c 32\""
    OIDCScope "openid email profile"
    # timeout for inactive session, default is 300 seconds
    OIDCSessionInactivityTimeout 86400
    # timeout for active session, default is 28800 seconds
    OIDCSessionMaxDuration 86400

    # non-existing URL for returning from OIDC server
    OIDCRedirectURI "/oauth2callback"
    <Location "/oauth2callback">
        AuthType openid-connect
        Require valid-user
    </Location>
    ```

3. If PKCE is supported, add the following:

    ```apacheconf
    OIDCPKCEMethod S256
    ```

4. Into the block of Apache configuration, which serves your application/website (e.g. the location block which contains ProxyPass), add the following for enabling any authenticate user:

    ```apacheconf
    AuthType openid-connect
    Require valid-user
    ```

     or if you need to perform some authorization, for example allow only users with existing username, use directives like these:

    ```apacheconf
    <Directory /var/www/wiki>
      AuthType openid-connect
      <RequireAll>
         Require claim "preferred_username~.+"
      </RequireAll>
      ErrorDocument 401 /missing-username.html
    </Directory>
    ```

## Additional resources

Sample configuration with additional settings options: [https://github.com/OpenIDC/mod_auth_openidc/blob/master/auth_openidc.conf](https://github.com/OpenIDC/mod_auth_openidc/blob/master/auth_openidc.conf)
