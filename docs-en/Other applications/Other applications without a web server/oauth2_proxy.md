# OAuth2 Proxy

A reverse proxy for connecting applications, websites or APIs that do not support OIDC authentication. It is possible to use it standalone or combined with nginx reverse proxy. If you are using Apache, use [mod_auth_openidc](../Apache%20web%20server/mod_auth_openidc.md) instead.

## Initial state

- This guide assumes that your server is located at `https://sp.example.org/` and that it is running an application or API with HTTP interface on port 8080.

## Registration information

- First perform the OIDC client registration, upon which you will recieve a **client_id** (YOUR_CLIENT_ID) and a **client_secret** (YOUR_CLIENT_SECRET). Use the following in the registration process:
    - **redirect_uri**: `https://sp.example.org/oauth2/callback`
    - **scopes**: openid, email
    - **authentication flow**: authorization code flow

## Configuration

1. Perform step 1 from the [linked guide](https://oauth2-proxy.github.io/oauth2-proxy/docs/).
2. Create a **/etc/oauth2-proxy.cfg** file with the following contents:

    ```ini
    upstreams = [
        "http://127.0.0.1:8080/"
    ]
    provider = "oidc"
    oidc_issuer_url = "%OIDC_ISSUER%"
    scope = "openid email"
    insecure_oidc_allow_unverified_email = true
    client_id = "YOUR_CLIENT_ID"
    client_secret = "YOUR_CLIENT_SECRET"
    insecure_oidc_skip_nonce = false
    session_cookie_minimal = true
    email_domains = "*"
    skip_provider_button = true
    whitelist_domains = [
        "%OIDC_HOSTNAME%"
    ]
    ```

3. The third step is dependent on whether you are using a reverse proxy.
    1. If you are using **nginx** as a **reverse proxy combined with OAuth2 Proxy**, add the following lines to the end of the file being created and then follow the [linked instructions](https://oauth2-proxy.github.io/oauth2-proxy/docs/configuration/overview#configuring-for-use-with-the-nginx-auth_request-directive) specific for this case.

        ```ini
        reverse_proxy = true
        http_address = "127.0.0.1:4180"
        set_xauthrequest = true
        ```

    2. If you are not using nginx and are running **only OAuth2 Proxy**, add the following lines to the end of the file being created (where **/opt/https-cert.pem** and \*_/opt/https-key.pem_ are the certificate and the key for HTTPS of your server sp.example.org/):

        ```ini
        tls_cert_file = "/opt/https-cert.pem"
        tls_key_file = "/opt/https-key.pem"
        http_address = "sp.example.com:80"
        https_address = "sp.example.com:443"
        ```

4. [Generate a cookie secret](https://oauth2-proxy.github.io/oauth2-proxy/docs/configuration/overview/#generating-a-cookie-secret), e.g. with this command:

    ```sh
    python -c 'import os,base64; print(base64.urlsafe_b64encode(os.urandom(32)).decode())'
    ```

5. Add the following address to the **/etc/oauth2-proxy.cfg** file, where YOUR_GENERATED_SECRET is your generated value.

    ```ini
    cookie_secret = "YOUR_GENERATED_SECRET"
    ```

6. If your application supports API and you want to enable its calling through OIDC bearer tokens, add this line to the **/etc/oauth2-proxy.cfg** file:

    ```ini
    skip_jwt_bearer_tokens = true
    ```

This will enable using tokens issued for the client_id of your application (YOUR_CLIENT_ID).

## Additional resources

- More configuration options: [https://oauth2-proxy.github.io/oauth2-proxy/docs/configuration/overview](https://oauth2-proxy.github.io/oauth2-proxy/docs/configuration/overview)
