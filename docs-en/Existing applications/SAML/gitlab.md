# GitLab

GitLab is an open source platform that allows source code management and analysis that can be run on your own server.

## Initial state

- This guide assumes that you already have an installed GitLab instance at `https://sp.example.org/`.

## Configuration

1. Allow SAML in the [described way](https://docs.gitlab.com/ee/integration/saml.html) according to your GitLab installation method. Use the following arguments and values:
    - `assertion_consumer_service_url: https://sp.example.org/users/auth/saml/callback`
    - `idp_cert_fingerprint: %SAML_SHA1_CERT_FINGERPRINT%`
    - `idp_sso_target_url: %SAML_SSO_HTTP_REDIRECT_URL%`
    - `issuer: https://sp.example.org/users/auth/saml/metadata`
    - `name_identifier_format: urn:oasis:names:tc:SAML:2.0:nameid-format:persistent`
2. Add the following to the arguments in the preceding step:
    - `attribute_statements: { email: ['urn:oid:0.9.2342.19200300.100.1.3'], first_name: ['urn:oid:2.5.4.42'], last_name: ['urn:oid:2.5.4.4'], name: ['urn:oid:2.16.840.1.113730.3.1.241'] }`
3. Optional: if you want to use an IdP attribute as an username in GitLab (typically uid), add the following attribute to **attribute_statements**:
    - `attribute_statements: { email: ['urn:oid:0.9.2342.19200300.100.1.3'], first_name: ['urn:oid:2.5.4.42'], last_name: ['urn:oid:2.5.4.4'], name: ['urn:oid:2.16.840.1.113730.3.1.241'], nickname: ['urn:oid:1.2.840.113549.1.9.2'] }`

## Registration information

- Your **entity ID** will be the same as your **metadata URL**: `https://sp.example.org/users/auth/saml/metadata`

## Additional resources

- More configuration options: [https://docs.gitlab.com/ee/integration/saml.html](https://docs.gitlab.com/ee/integration/saml.html)
