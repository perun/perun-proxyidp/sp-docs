# Sentry

Sentry is an open source instrument for performance monitoring and error tracking that can be run on your own server.

## Initial state

- This guide assumes that you already have a [set up](https://develop.sentry.dev/self-hosted/) self-hosted Sentry at `https://sp.example.org/` and that you have already created and organization.
    - If you are using multiple organizations in Sentry, you need to connect each one individually.
    - If you are using sentry.io, replace `https://sp.example.org/` in this guide with `https://sentry.io/`.

## Configuration

1. In Sentry administration select **User Authentication > SSO**.
2. Enter %SAML_METADATA_URL% in the **Metadata URL** field.
3. Press the **Get metadata** button.
    - If there is an error, Sentry most likely does not have internet access (e.g. due to the firewall). In that case, follow the steps below:
        1. Select the **XML** tab.
        2. Download the contents of %SAML_METADATA_URL% and paste them in the **Metadata XML** field.
        3. Confirm through the **Parse Metadata** button.
4. On the following **Map Identity Provider Attributes** page, fill in the fields like so:
    1. **IdP User ID**: `urn:oid:1.3.6.1.4.1.5923.1.1.1.13`
    2. **User Email**: `urn:oid:0.9.2342.19200300.100.1.3`
    3. **First Name**: `urn:oid:2.5.4.42`
    4. **Last Name**: `urn:oid:2.5.4.4`

## Registration information

- Your **entity ID** will be the same as your **metadata URL**: `https://sentry.io/saml/metadata/[organization_slug]/`, where `[organization_slug]` is the slug of your organization.

## Additional resources

- More information on the integration process: [https://docs.sentry.io/product/accounts/sso/saml2/](https://docs.sentry.io/product/accounts/sso/saml2/)
