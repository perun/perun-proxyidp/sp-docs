# Instructions

Many services include support for external authentication by an identity provider through SAML2 or OpenID Connect authentication protocols. However, configuration is often complex in practice, varies from case to case, and requires at least a basic understanding of the authentication protocol used. This documentation describes the appropriate configuration of selected software products.

## Configuration guides

First you need to choose the guide that fits your specific case:

### ![](icons/desktop-solid.png) Frontend applications (e.g. JavaScript)

- [Expo AuthSession](docs-en/Frontend%20applications/OIDC/expo_authsession.md) (OIDC)

### ![](icons/code-solid.png) Own server-side applications (e.g. PHP, Python)

- OIDC + PHP: [PHP OpenID Connect Basic Client](docs-en/Own%20server-side%20applications/OIDC%20%2B%20PHP/php_openid_connect_basic_client.md)
- OIDC + Python Django: [mozilla-django-oidc](docs-en/Own%20server-side%20applications/OIDC%20%2B%20Python%20Django/mozilla-django-oidc.md)
- SAML + PHP: [SimpleSAMLphp](docs-en/Own%20server-side%20applications/SAML%20%2B%20PHP/simplesamlphp.md)

### ![](icons/server-solid.png) Existing self-hosted applications

- [GitLab](docs-en/Existing%20applications/SAML/gitlab.md) (SAML)
- [Sentry](docs-en/Existing%20applications/SAML/sentry.md) (SAML)

### ![](icons/globe-solid.png) Other applications without OIDC or SAML integration

#### Apache web server

- [mod_auth_openidc](docs-en/Other%20applications/Apache%20web%20server/mod_auth_openidc.md) (OIDC)
- [mod_oauth2](docs-en/Other%20applications/Apache%20web%20server/mod_oauth2.md) (OAuth)
- [Shibboleth SP 3](docs-en/Other%20applications/Apache%20web%20server/shibboleth_sp.md) (SAML)

#### Other applications without a web server

- [OAuth2 Proxy](docs-en/Other%20applications/Other%20applications%20without%20a%20web%20server/oauth2_proxy.md)
